﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Banking_Kata.test
{
    [TestFixture]
    public class TransactionRepositoryShould
    {
        private const string TODAY_DATE = "21/08/2018";
        private const string PREVIOUS_DATE = "21/07/2018";
        private ITransactionRepository _transactionRepository;
        private static readonly int _withdrawalAmount = 300;
        private static readonly int _depositAmount = 500;
        private Transaction _depositTransaction, _withdrawalTransaction;

        [SetUp]
        public void Setup()
        {
            _transactionRepository = new TransactionRepository();
            _depositTransaction = new Transaction(_depositAmount, PREVIOUS_DATE);
            _withdrawalTransaction = new Transaction(-_withdrawalAmount, TODAY_DATE);
        }

        [TearDown]
        public void TearDown()
        {
            _transactionRepository = null;
            _depositTransaction = null;
            _withdrawalTransaction = null;
        }

        [Test]
        public void contains_a_stored_deposit()
        {
            _transactionRepository.StoreDeposit(_depositAmount, PREVIOUS_DATE);

            CollectionAssert.Contains(_transactionRepository.GetTransactions(), _depositTransaction);
        }

        [Test]
        public void contains_a_stored_withdrawal()
        {
            _transactionRepository.StoreWithdrawal(_withdrawalAmount, TODAY_DATE);

            CollectionAssert.Contains(_transactionRepository.GetTransactions(), _withdrawalTransaction);
        }

        [Test]
        public void contains_all_stored_transactions()
        {
            StoreTwoTransactions();
            Assert.True(_transactionRepository.GetTransactions().Count() == 2);
        }

        [Test]
        public void return_stored_transactions_in_reverse_order()
        {
            StoreTwoTransactions();
            var orderedListOfTransactions = GenerateOrderedListOfTransactions();

            List<Transaction> transactions = _transactionRepository.GetTransactions().ToList();

            Assert.AreEqual(orderedListOfTransactions, transactions);
        }

        private List<Transaction> GenerateOrderedListOfTransactions()
        {
            var transactions = new List<Transaction> {_depositTransaction, _withdrawalTransaction};
            transactions = transactions.OrderByDescending(t => t.Date).ToList();
            return transactions;
        }

        private void StoreTwoTransactions()
        {
            _transactionRepository.StoreDeposit(_depositAmount, PREVIOUS_DATE);
            _transactionRepository.StoreWithdrawal(_withdrawalAmount, TODAY_DATE);
        }
    }
}

