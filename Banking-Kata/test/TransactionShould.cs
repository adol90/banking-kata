﻿using NUnit.Framework;

namespace Banking_Kata.test
{
    [TestFixture]
    public class TransactionShould
    {
        private const int Amount = 500;
        private const int AnotherAmount = -100;
        private const string Date = "today";
        private const string AnotherDate = "notToday";

        [Test]
        public void create_different_transactions_generates__different_transactions_as_expected()
        {
            var transaction = new Transaction(Amount, Date);
            var anotherTransaction = new Transaction(AnotherAmount,AnotherDate);

            Assert.IsFalse(transaction.Equals(anotherTransaction));

        }
    }
}
