﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;

namespace Banking_Kata.test
{
    [TestFixture]
    public class AccountShould
    {
        private const string TODAY_DATE = "21/08/2018";
        private const string INTERMEDIATE_DATE = "15/08/2018";
        private const string PREVIOUS_DATE = "21/07/2018";
        private IEnumerable<Transaction> _transactionsList;
        private Account _account;
        private Mock<ITransactionRepository> _repository;
        private Mock<ILogger> _logger;
        private IDate _dateGenerator;
        private static int _withdrawalAmount = 100;
        private static int _depositAmount = 500;
        private static string _todaysDate;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<ITransactionRepository>();
            _logger = new Mock<ILogger>();
            _dateGenerator = new Date();
            _todaysDate = _dateGenerator.TodayAsString();
            _transactionsList = new List<Transaction>()
            {
                new Transaction(_depositAmount,PREVIOUS_DATE),
                new Transaction(_withdrawalAmount, TODAY_DATE)
            };
            _repository.Setup(m => m.StoreDeposit(_depositAmount, _todaysDate));
            _repository.Setup(m => m.StoreWithdrawal(_withdrawalAmount, _todaysDate));
            _repository.Setup(m => m.GetTransactions()).Returns(_transactionsList);
            _account = new Account(_repository.Object, _logger.Object, _dateGenerator);
        }

        [Test]
        public void store_a_deposit_transaction()
        {
            _account.Deposit(_depositAmount);

            _repository.Verify(m => m.StoreDeposit(_depositAmount, _todaysDate));
        }

        [Test]
        public void store_a_withdrawal_transaction()
        {
            _account.Withdrawal(_withdrawalAmount);

            _repository.Verify(m => m.StoreWithdrawal(_withdrawalAmount, _todaysDate));
        }

        [Test]
        public void print_all_statements()
        {
            _account.PrintStatement();
            int balance = _transactionsList.First().Amount;
            int index = 0;

            _logger.Verify(m => m.PrintLine("Date   Amount   Balance"));
            foreach (var transaction in _transactionsList)
            {
                _logger.Verify(m => m.PrintLine("" + transaction.Date + "   " + transaction.Amount + "   " + balance));
                balance = NextTransaction(index, balance);
            }

        }

        private int NextTransaction(int index, int balance)
        {
            index++;
            return balance + _transactionsList.ElementAt(index).Amount;

        }
    }
}
