﻿using Moq;
using NUnit.Framework;

namespace Banking_Kata.test
{
    [TestFixture]
    public class AcceptanceTest
    {
        //private ILogger _logger;
        private ITransactionRepository _transactionRepository;
        private Date _dateGenerator;
        private Account _account;
        private Mock<ILogger> _logger;


        [SetUp]
        public void Setup()
        {
            _logger = new Mock<ILogger>();
            _logger.Setup(m => m.PrintLine(It.IsAny<string>()));
            _dateGenerator = new Date();
            _transactionRepository = new TransactionRepository();
            _transactionRepository.StoreDeposit(500, "09/08/2018");
            _account = new Account(_transactionRepository, _logger.Object, _dateGenerator);
        }

        [TearDown]
        public void TearDown()
        {
            _logger = null;
            _transactionRepository = null;
            _account = null;
        }

        [Test]
        public void should_print_balance()
        {
            _account.PrintStatement();
            _logger.Verify(m => m.PrintLine(It.IsAny<string>()));
        }
        
    }
}
