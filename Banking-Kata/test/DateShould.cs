﻿using System;
using NUnit.Framework;

namespace Banking_Kata.test
{
    public class DateShould
    {
        private Date _dateGenerator;

        [SetUp]
        public void Setup()
        {
            _dateGenerator = new Date();
        }

        [Test]
        public void date_todayAsString_method_returns_todays_date_in_correct_format()
        {
            var todaysDate = _dateGenerator.TodayAsString();
            Assert.AreEqual(todaysDate, DateTime.Today.ToShortDateString());
        }

    }

}
