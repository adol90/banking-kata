﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banking_Kata
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            ITransactionRepository repository = new TransactionRepository();
            ILogger logger = new Logger();
            IDate dateGenerator = new Date();

            Account account = new Account(repository, logger, dateGenerator);
            account.Deposit(500);
            account.Withdrawal(100);
            account.PrintStatement();

            Console.ReadLine();


        }
    }
}
