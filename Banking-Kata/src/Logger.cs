﻿using System;

namespace Banking_Kata
{
    public interface ILogger
    {
        void PrintLine(string line);
    }

    public class Logger : ILogger
    {
        public void PrintLine(string line)
        {
            Console.WriteLine(line);
        }
    }
}
