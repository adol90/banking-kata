﻿using System;

namespace Banking_Kata
{
    public class Date : IDate
    {
        public string TodayAsString()
        {
            return DateTime.Today.ToShortDateString();
        }
    }

    public interface IDate
    {
        string TodayAsString();
    }
}