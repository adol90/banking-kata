﻿namespace Banking_Kata
{
    public class Transaction
    {
        public readonly int Amount;
        public string Date { get; }

        public Transaction(int amount, string date)
        {
            Date = date;
            Amount = amount;
        }

        protected bool Equals(Transaction other)
        {
            return Amount == other.Amount && string.Equals(Date, other.Date);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Transaction) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Amount * 397) ^ (Date != null ? Date.GetHashCode() : 0);
            }
        }

    }
}