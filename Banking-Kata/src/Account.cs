﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Banking_Kata
{
    public class Account
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ILogger _logger;
        private readonly IDate _dateGenerator;

        public Account(ITransactionRepository transactionRepository, ILogger logger, IDate dateGenerator)
        {
            _logger = logger;
            _transactionRepository = transactionRepository;
            _dateGenerator = dateGenerator;
        }


        public void Deposit(int amount)
        {
            _transactionRepository.StoreDeposit(amount, _dateGenerator.TodayAsString());
        }

        public void Withdrawal(int amount)
        {
            _transactionRepository.StoreWithdrawal(amount, _dateGenerator.TodayAsString());
        }

        public void PrintStatement()
        {
            _logger.PrintLine("Date   Amount   Balance");
            IEnumerable<Transaction> repositoryTransactions = _transactionRepository.GetTransactions();
            int currentBalance = repositoryTransactions.First().Amount;

            foreach (var transaction in repositoryTransactions)
            {
                currentBalance = GetCurrentBalance(transaction, repositoryTransactions, currentBalance);
                _logger.PrintLine(transaction.Date + "   " + transaction.Amount + "   " + currentBalance);
            }
        }

        private int GetCurrentBalance(Transaction transaction, IEnumerable<Transaction> repositoryTransactions, int currentBalance)
        {
            if (!Equals(transaction, repositoryTransactions.First()))
            {
                currentBalance += transaction.Amount;
            }

            return currentBalance;
        }
    }
}
