﻿using System.Collections.Generic;
using System.Linq;

namespace Banking_Kata
{
    public interface ITransactionRepository
    {
        void StoreDeposit(int amount, string date);
        void StoreWithdrawal(int amount, string date);
        IEnumerable<Transaction> GetTransactions();
    }

    public class TransactionRepository : ITransactionRepository
    {
        private readonly List<Transaction> _transactions;

        public TransactionRepository()
        {
            _transactions = new List<Transaction>();
        }

        public void StoreDeposit(int amount, string date)
        {
            _transactions.Add( new Transaction(amount, date));
        }

        public void StoreWithdrawal(int amount, string date)
        {
            _transactions.Add(new Transaction(-amount, date));
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            return _transactions.OrderByDescending(t => t.Date).AsEnumerable();
        }
    }
}